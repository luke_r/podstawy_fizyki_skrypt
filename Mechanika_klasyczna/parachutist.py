#!/usr/bin/env python3

import math
import numpy as np
import matplotlib.pyplot as plt

plot_file = "parachutist.pdf"

g = 9.80665 # m/s^2
m = 90 # kg
cd = 1.3
rho = 1.225 # kg/m^3
h0 = 20 # m
diameters = (5, 6, 7, 8, 10, 15) # m

v_scale = 3.6 # m/s to km/h

def h_free(t):
    #return h0 - g * t**2 / 2
    return h0 - g * t*t / 2

def v_free(t):
    return g * t

def lambd(d):
    return d * np.sqrt(np.pi * cd * rho * g / (2 * m))

def v_inf(d):
    return (2 / d * np.sqrt(2 * m * g / (np.pi * cd * rho)))

def h(t, d):
    lambda_d = lambd(d)
    v_inf_d = v_inf(d)
    return (
        h0 - v_inf_d * (
            2 / lambda_d
            * np.log((np.exp(lambda_d * t) + 1) / 2)
            -t
        )
    )

def v(t, d):
    lambda_d = lambd(d)
    v_inf_d = v_inf(d)
    return (
        (np.exp(lambda_d * t) - 1)
        / (np.exp(lambda_d * t) + 1)
        * v_inf_d
    )

t_min = 0
t_max = 4
step_count = 100
t = np.linspace(t_min, t_max, step_count)
h_min = 0
h_max = h0
v_min = 0
v_max = v_scale * v_inf(min(diameters))

plt.rcParams["figure.figsize"][1] *= 1.5
fig, axs = plt.subplots(nrows = 2, sharex = True)

for ax in axs:
    ax.axhline(color = "black")
    ax.axvline(color = "black")

axs[1].set_xlim(t_min, t_max)
axs[1].set_xlabel(r"$t/\mathrm{s}$")
xtic_count = 3
xtics = np.linspace(t_min, t_max, xtic_count)
axs[1].set_xticks(xtics)

axs[0].set_ylim(h_min, h_max)
axs[0].set_ylabel(r"$h/\mathrm{m}$")

axs[1].set_ylim(v_min, v_max)
axs[1].set_ylabel(r"$v/(\mathrm{km}/\mathrm{h})$")

iplot = 0
colors = ("red", "green", "blue", "magenta", "cyan", "black")
free_fall_color = "black"
free_fall_linestyle = "--"
for d in diameters:
    axs[0].plot(t, h(t, d), color = colors[iplot], label = r"$" + str(d) + "$")
    axs[1].plot(t, v_scale * v(t, d), color = colors[iplot])
    iplot = iplot + 1

axs[0].plot(
    t, h_free(t),
    color = free_fall_color,
    linestyle = free_fall_linestyle,
    label = "spadek swobodny")
axs[1].plot(
    t, v_scale * v_free(t),
    color = free_fall_color,
    linestyle = free_fall_linestyle)

legend = axs[0].legend(frameon = False, title = r"$d/\mathrm{m}$")
legend._legend_box.align = "left"
fig.tight_layout()

plt.savefig(plot_file)
