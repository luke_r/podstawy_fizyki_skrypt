#!/usr/bin/env python3

import numpy as np
from scipy.special import comb
import matplotlib.pyplot as plt

plot_file = "gas_cylinder.pdf"

def prob(N, n):
    return comb(N, n) / 2**N

N = 1000
n_min = 0
n_max = N
n_step = 1
n = np.arange(n_min, n_max + 1, n_step)
p = prob(N, n)
p_min = 0
p_max = np.amax(p)

fig, ax = plt.subplots()

ax.axhline(color = "black")
ax.axvline(color = "black")

ax.set_xlim(n_min, n_max)
ax.set_xlabel(r"$n$")
xtic_count = 5
xtics = np.linspace(n_min, n_max, xtic_count)
ax.set_xticks(xtics)

ax.set_ylim(p_min, p_max)
ax.set_ylabel(r"$p(n)$")

ax.plot(n, p, color = "red")
fig.tight_layout()
plt.savefig(plot_file)
