#!/usr/bin/env python3

import math
import numpy as np
import matplotlib.pyplot as plt

plot_file = "derivative.pdf"

def f(x):
    return np.sin(x) * np.exp(-np.abs(x))

x_min = -5
x_max = 5
step_count = 100

x = np.linspace(x_min, x_max, step_count)
y = f(x)
# y = np.sin(x) * np.exp(-np.abs(x))

fig, ax = plt.subplots()

ax.axhline(color = "black")
ax.axvline(color = "black")

ax.set_xlim(x_min, x_max)
ax.set_xlabel(r"$x$")
ax.set_ylabel(r"$y$")
ytic_count = 5
#ytics = np.linspace(y_min, y_max, ytic_count)
#ax.set_yticks(ytics)

#xtic_mask = np.ones(xtic_count, dtype = bool)
#xtic_mask[[0, xtic_count - 1]] = False
x0 = -1
ax.vlines(x0, 0, f(x0), linestyles = "dashed")
ax.hlines(f(x0), 0, x0, linestyles = "dashed")

plt.plot(x, y, color = "red", label = r"$\sin{(x)}$")
#ax.legend()

plt.savefig(plot_file)
