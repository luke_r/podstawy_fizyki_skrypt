#!/usr/bin/env python3

import math
import numpy as np
from scipy.stats import norm
import matplotlib.pyplot as plt

plot_file = "height_distribution.pdf"

mu, sigma, people_count = 170, 10, 1000
heights = np.random.normal(mu, sigma, people_count)
height_min = math.ceil(heights.min())
height_max = math.floor(heights.max())
height = np.linspace(height_min, height_max, 1000)
height_pdf = people_count * norm.pdf(height, mu, sigma)
bin_count = height_max - height_min

fig, ax = plt.subplots()

ax.set_xlim(height_min, height_max)
ax.set_xlabel("wzrost/cm")
ax.set_ylabel("liczba ludzi")
ax.axhline(color = "black")

plt.hist(
    heights,
    bin_count,
    label = "histogram",
    color = "white",
    edgecolor = "black"
    # alpha = 0.5
)
plt.plot(
    height, height_pdf,
    linewidth = 2,
    label = r"skalowana krzywa Gaussa: $\mu = " + str(mu) + "$ cm, $\sigma = " + str(sigma) + "$ cm",
    color = "black"
)

ax.legend()
plt.savefig(plot_file)
