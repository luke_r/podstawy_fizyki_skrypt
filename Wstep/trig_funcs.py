#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter

plot_file = "trig_funcs.pdf"

x_min_pi = 0
x_max_pi = 2
step_count = 100

x_min = x_min_pi * np.pi
x_max = x_max_pi * np.pi

x = np.linspace(x_min, x_max, step_count)
sin_x = np.sin(x)
cos_x = np.cos(x)

all_y = np.concatenate((sin_x, cos_x))
y_min = np.amin(all_y)
y_max = np.amax(all_y)

def pi(val, pos):
    # value and tick position
    return "{:.1f}".format(val / np.pi)

formatter = FuncFormatter(pi)

fig, ax = plt.subplots()

ax.set_xlim(x_min, x_max)
ax.set_xlabel(r"$x / \pi$")
xtic_count = 5
xtics = np.linspace(x_min, x_max, xtic_count)
ax.set_xticks(xtics)
ax.xaxis.set_major_formatter(formatter)

ax.set_ylim(y_min, y_max)
ax.set_ylabel(r"$y$")
ytic_count = 5
ytics = np.linspace(y_min, y_max, ytic_count)
ax.set_yticks(ytics)

ax.axhline(color = "black")
xtic_mask = np.ones(xtic_count, dtype = bool)
xtic_mask[[0, xtic_count - 1]] = False
ax.vlines(xtics[xtic_mask], y_min, y_max, linestyles = "dotted")

plt.plot(x, sin_x, color = "red", label = r"$\sin{(x)}$")
plt.plot(x, cos_x, color = "blue", label = r"$\cos{(x)}$")
ax.legend()

plt.savefig(plot_file)
