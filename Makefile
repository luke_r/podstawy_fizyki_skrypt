book_name := Podstawy_fizyki_skrypt
book_main_src := $(book_name).tex
book_pdf := $(book_name).pdf
section_aux_dirs := $(wildcard */)

all : $(book_pdf)

.PHONY : $(section_aux_dirs)
$(section_aux_dirs) :
	$(MAKE) -C $@ $(MAKECMDGOALS)

.PHONY : force_make

$(book_pdf) : $(book_main_src) $(section_aux_dirs) force_make
	latexmk -pdf $<

view : $(book_pdf)
	xdg-open $<

.PHONY : clean
clean : $(section_aux_dirs)
	latexmk -bibtex-cond1 -C
	$(RM) ./*.ist ./*.run.xml
