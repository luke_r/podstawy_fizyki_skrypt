\chapter{Wstęp}

\section{Fizyka na co dzień}

\emph{Fizyka} to odpowiedź na \emph{ciekawość świata} w pięknym języku matematyki. Rozejrzyjmy się wokół siebie i zadajmy parę pytań o otaczający nas świat.
\begin{itemize}
	\item Dlaczego niebo:
	\begin{itemize}
		\item w słoneczny dzień jest niebieskie?
		\item przy zachodzie i wschodzie czerwonawe?
		\item w nocy przy bezchmurnej pogodzie czarne?
	\end{itemize}
	\item Skąd biorą się fazy Księżyca?
	\item Jak powstaje zorza polarna?
	\item Dlaczego:
	\begin{itemize}
		\item występują pory roku?
		\item dwa razy w ciągu doby występują pływy?
		\item prawidłowo wyrzucony bumerang wraca do rzucającego?
		\item samolot lata?
		\item pranie schnie szybciej na wietrze?
		\item studzimy gorącą herbatę dmuchaniem?
		\item na jesieni często tworzą się mgły?
		\item im wyżej wyjdziemy, tym niższa temperatura?
		\item powinniśmy się trzymać uchwytu stojąc w autobusie, który rusza, hamuje, lub pokonuje zakręt?
		\item w upał asfaltowa droga zdaje się falować?
		\item gwiazdy na niebie migoczą, a planety --- nie?
	\end{itemize}
	\item Na jakiej zasadzie działa:
	\begin{itemize}
		\item lodówka?
		\item klimatyzacja?
		\item płyta indukcyjna?
		\item mikrofalówka?
		\item policyjna \emph{suszarka}?
		\item odcinkowy pomiar prędkości?
		\item \acrfull{abs}?
		\item \acrfull{usg}?
		\item tomografia komputerowa?
		\item rezonans magnetyczny?
	\end{itemize}
\end{itemize}
Oczywiście na razie na te pytania nie musimy odpowiadać. Warto jednak w toku studiów poznać odpowiedzi na niektóre z nich.

\section{Duże i małe odległości}

Rozpiętość rozmiarów obiektów będących przedmiotem badań fizyki jest niewyobrażalna. Przykłady obiektów od wzrostu człowieka w górę i w dół skali zebrano w tab.~\ref{tab:wstep_odleglosci}

\begin{table}[H]
	\centering
	\caption{Rzędy odległości we Wszechświecie}
	\begin{tabular}{>{$}l<{$}l}
		\toprule
		\multicolumn{1}{c}{odległość/\si{m}} & obiekt \\
		\midrule
		1 & parasol \\
		\midrule
		\multicolumn{2}{c}{\textbf{coraz większe odległości}} \\
		\midrule
		10 & dom \\
		100 & blok mieszkalny \\
		1000 & najwyższy drapacz chmur na świecie \\
		10^4 & wysokość przelotowa samolotów \\
		10^8 & \earth \ldots \leftmoon (Księżyc) \\
		10^{10} & \earth \ldots \venus (Wenus) \\
		10^{11} & \earth \ldots \astrosun (Słońce) \\
		10^{12} & \astrosun \ldots \jupiter (Jowisz) \\
		10^{13} & \astrosun \ldots \pluto (Pluton) \\
		10^{16} & \astrosun \ldots Proxima Centauri \\
		10^{18} & \astrosun \ldots Polaris (północna Gwiazda Polarna) \\
		10^{20} & \astrosun \ldots jądro Drogi Mlecznej \\
		10^{22} & \astrosun \ldots Galaktyka Andromedy \\
		10^{26} & \astrosun \ldots najdalsze obserwowane kwazary \\
		\midrule
		\multicolumn{2}{c}{\textbf{coraz mniejsze odległości}} \\
		\midrule
		0.1	& pióro wieczne\footnote{Tylko barbarzyńcy piszą długopisami.} \\
		0.01 & moneta \\
		0.001 & milimetrowy robaczek w kuchni \\
		10^{-4} & grubość kartki papieru, ameba \\
		10^{-6} & komórka, bakteria \\
		10^{-7} & wirus \\
		10^{-8} & cząsteczka białka \\
		10^{-9} & klaster cząsteczek \ce{H2O} \\
		10^{-10} & atom wodoru \ce{H} \\
		10^{-14} & duże jądro atomowe \\
		10^{-15} & proton \\
		10^{-19} & kwark (?) \\
		\bottomrule
	\end{tabular}
	\label{tab:wstep_odleglosci}
\end{table}

\section{Adres zamieszkania}

Poznawszy zgrubnie odległości we Wszechświecie, warto określić nieco dokładniej nasze położenie:
\begin{center}
	\tiny{Ziemia} \\
	\small{Układ Słoneczny} \\
	\Large{Ramię Oriona} \\
	\huge{Droga Mleczna} \\
	\Huge{Grupa Lokalna Galaktyk}
\end{center}
Warto spojrzeć też na jedno z najsłynniejszych zdjęć wykonanych w historii, pokazane na rys.~\ref{fig:wstep_palebluedot}.
\begin{figure}[h!]
	\centering
	\includegraphics[width = 0.6\textwidth]{Wstep/palebluedot}
	\caption{\textit{\foreignlanguage{english}{Pale Blue Dot}}, zdjęcie Ziemi wykonane przez sondę~\textit{Voyager 1} spoza orbity Plutona}
	\label{fig:wstep_palebluedot}
\end{figure}

\section{Wielkości fizyczne}

\emph{Wielkości fizyczne} są właściwościami ciał, które można określić ilościowo:
\begin{itemize}
	\item skalarne --- mają tylko wartość, a nie kierunek. Oznaczane są zwykłą pochyloną czcionką, np.~$T$~(temperatura), $m$~(masa), $q$~(ładunek elektryczny).
	\item wektorowe --- mają wartość oraz kierunek. Oznaczane są prostą pogrubioną czcionką\footnote{W piśmie odręcznym wygodniej oznaczać je strzałką nad literą zamiast pogrubioną czcionką, np.~$\vec{v}$ zamiast~$\vb{v}$.}, np.~$\vb{v}$~(prędkość), $\vb{F}$~(siła), $\vb{B}$~(indukcja magnetyczna).
\end{itemize}
Wielkości skalarne nie zależą od wyboru układu współrzędnych, w przeciwieństwie do wielkości wektorowych.
\section{Trochę matematyki}

\subsection{Wektory}

\emph{Wektor} w fizyce oznacza obiekt mający wielkość~(\emph{moduł}) oraz zwrot, zwykle opisany trójką współrzędnych:
\begin{equation}
	\vb{a} =
		\begin{bmatrix}
			a_x \\
			a_y \\
			a_z
		\end{bmatrix},
	\label{eq:wstep_wektor}
\end{equation}
choć często rozważać będziemy wektory na płaszczyźnie (opisane dwójką współrzędnych) bądź linii prostej (opisane tylko jedną współrzędną). Współrzędne wektora zależą od wyboru układu współrzędnych, natomiast jego moduł (norma, wartość) --- nie:
\begin{equation}
	\norm{\vb{a}} \equiv a = \sqrt{a_x^2 + a_y^2 + a_z^2}.
	\label{eq:wstep_norma_wekt}
\end{equation}
Istotne jest, że współrzędne wektora mogą być dowolnymi liczbami,
\begin{equation}
	a_x, a_y, a_z \in \mathbb{R},
\end{equation}
natomiast moduł wektora jest zawsze nieujemny,
\begin{equation}
	a \ge 0.
	\label{eq:wstep_modul_wekt_nieujemny}
\end{equation}
W szczególności \emph{wektor zerowy} jest wektorem o zerowych współrzędnych,
\begin{equation}
	\vb{0} =
		\begin{bmatrix}
			0 \\
			0 \\
			0
		\end{bmatrix}
		\label{eq:wstep_wektor_zerowy}
\end{equation}
i jest to jedyny wektor o zerowym module,
\begin{equation}
	\norm{\vb{0}} = 0.
\end{equation}
Dla dwóch wektorów~$\vb{a}$ i~$\vb{b}$ definiujemy ponadto \emph{iloczyn skalarny},
\begin{align}
	\vb{a} \vdot \vb{b}	& = a_x b_x + a_y b_y + a_z b_z \label{eq:wstep_iloczyn_skalarny_anal} \\
	 & = a b \cos{\varphi} \label{eq:wstep_iloczyn_skalarny_modul}, \\
\end{align}
gdzie~$\varphi$ jest kątem między wektorami~$\vb{a}$ i~$\vb{b}$. Łącząc~\eqref{eq:wstep_norma_wekt} i~\eqref{eq:wstep_iloczyn_skalarny_anal}, uzyskujemy
\begin{equation}
	a = \sqrt{\vb{a} \vdot \vb{a}}.
	\label{eq:wstep_modul_wekt_iloczyn_skalarny}
\end{equation}
\emph{Wersorem} nazywamy wektor jednostkowy (tzn. o jednostkowej długości). Dla każdego niezerowego wektora można utworzyć wersor o zwrocie tego wektora:
\begin{equation}
	\vu{a} = \frac{\vb{a}}{a}.
	\label{eq:wstep_wersor}
\end{equation}
Istotnie, z~\eqref{eq:wstep_modul_wekt_nieujemny} i~\eqref{eq:wstep_modul_wekt_iloczyn_skalarny} otrzymujemy
\begin{align}
	\norm{\vu{a}} & = \frac{\sqrt{\vb{a} \vdot \vb{a}}}{a} \\
	 & = 1.
\end{align}

\subsection{Pochodne}

W kursie fizyki niezbędna jest wiedza o zmienności funkcji, w szczególności o ilościowym sposobie jej określania. Spójrzmy na wykres funkcji~$f(x) = \sin{x}$ na rys.~\ref{fig:wstep_sin_cos}.
\begin{figure}[h!]
	\centering
	\includegraphics{Wstep/trig_funcs}
	\caption{Wykres funkcji~$\sin{x}$ i~$\cos{x}$ dla~$x \in \interval{0}{2 \pi}$.}
	\label{fig:wstep_sin_cos}
\end{figure}
Funkcja~$\sin{x}$ począwszy od~$x = 0$ rośnie początkowo szybko, potem coraz wolniej, wreszcie osiąga największą wartość dla~$x = \frac{\pi}{2}$. Następnie zaczyna maleć, najpierw wolno, potem szybciej, znowu wolno, aż osiąga wartość najmniejszą dla~$x = \frac{3}{2} \pi$.

Naszym celem jest teraz ilościowy opis szybkości zmian funkcji dla ustalonego argumentu~$x_0$. Intuicyjnie szybkość zmiany to wielkość tej zmiany przez czas, w jakim zmiana zaszła. W naszym przypadku rolę czasu zmiany wartości funkcji pełni przyrost argumentu~$x$, który oznaczymy przez~$\Delta x$. Jeśli~$x$ zmieni się o~$\Delta x$ w punkcie~$x_0$, funkcja zmieni się o
\begin{equation}
	\Delta f(x_0, \Delta x) = f(x_0 + \Delta x) - f(x_0),
\end{equation}
a szybkość zmiany to
\begin{align}
	\bar{v}_f(x_0, \Delta x) & = \frac{\Delta f(x_0, \Delta x)}{\Delta x} \\
	 & = \frac{f(x_0 + \Delta x) - f(x_0)}{\Delta x}. \label{eq:wstep_szybkosc_zmiany_funkcji}
\end{align}
W matematyce szybkość~\eqref{eq:wstep_szybkosc_zmiany_funkcji} nazywamy \emph{ilorazem różnicowym}. Zauważmy, że iloraz~\eqref{eq:wstep_szybkosc_zmiany_funkcji} zależy jednak nie tylko od argumentu~$x_0$, ale i od jego przyrostu~$\Delta x$, co ilustruje tab.~\ref{tab:wstep_sin_przyrost}.
\begin{table}[H]
	\centering
	\caption{Szybkość przyrostu funkcji~$\sin{x}$.}
	\begin{tabular}{*{3}{>{$}c<{$}}>{$}r<{$}}
		\toprule
		x_0 & \Delta x & \Delta \sin{(x_0, \Delta x)} = \sin{(x_0 + \Delta x)} - \sin{x_0} & \bar{v}_{\sin{x}} = \frac{\Delta \sin{(x_0, \Delta x)}}{\Delta x} \\
		\midrule
		0 & \frac{\pi}{6} & \frac{1}{2} & \frac{3}{\pi} \approx 0.95 \\
		0 & \frac{\pi}{3} & \frac{\sqrt{3}}{2} & \frac{3 \sqrt{3}}{2 \pi} \approx 0.83 \\
		0 & \frac{\pi}{2} & 1 & \frac{2}{\pi} \approx 0.64 \\
		0 & \pi & 0 & 0 \\
		\midrule
		\frac{\pi}{2} & \frac{\pi}{6} & \frac{\sqrt{3}}{2} - 1 & \approx -0.26 \\
		\frac{\pi}{2} & \frac{\pi}{3} & -\frac{1}{2} & -\frac{3}{2 \pi} \approx -0.48 \\
		\frac{\pi}{2} & \frac{\pi}{2} & -1 & -\frac{2}{\pi} \approx -0.64 \\
		\bottomrule
	\end{tabular}
	\label{tab:wstep_sin_przyrost}
\end{table}
Dobrze byłoby znaleźć miarę szybkości zmiany funkcji w danym punkcie niezależną od przyrostu argumentu, a więc zależną tylko od wyboru argumentu~$x_0$. Problem ten odpowiada znanemu z codziennego życia pomiarowi prędkości. Znając przejechaną odległość~$\Delta s$ i czas przejazdu~$\Delta t$ możemy obliczyć średnią prędkość na trasie:
\begin{equation}
	\bar{v} = \frac{\Delta s}{\Delta t}.
	\label{tab:wstep_predk_srednia}
\end{equation}
Wzór~\eqref{tab:wstep_predk_srednia} opisuje jednak \emph{prędkość średnią}, która, podobnie jak szybkość zmiany funkcji~\eqref{eq:wstep_szybkosc_zmiany_funkcji}, zależy od wyboru początku i długości trasy, na której jest obliczana. Dla kierowcy bardziej interesująca jest jednak prędkość, jaką ma w danej chwili, bo taka prędkość jest mierzona przez rozmaite służby. Jak więc wyeliminować zależność od przebytej drogi --- tak, by uzyskać \emph{prędkość chwilową}, i ogólniej --- szybkość przyrostu funkcji dla danego argumentu niezależną od jego przyrostu? Otóż rozwiązanie problemu polega na zmniejszeniu przyrostu prawie do zera --- tak, by przyrost był \emph{infinitezymalnie} (nieskończenie mało) większy od zera. W języku matematyki oznacza to obliczenie \emph{granicy}, a szybkość przyrostu w granicy zerowego przyrostu nazywamy \emph{pochodną}:
\begin{align}
	f'(x_0) & = \lim_{\Delta x \to 0} \bar{v}_f(x_0, \Delta x) \\
	 & = \lim_{\Delta x \to 0} \frac{f(x_0 + \Delta x) - f(x_0)}{\Delta x} \label{eq:wstep_pochodna_granica} \\
	 & \equiv \dv{f(x_0)}{x} \label{eq:wstep_pochodna_rozniczki}.
\end{align}
Notacja~\eqref{eq:wstep_pochodna_rozniczki} jest uproszczonym zapisem granicy~\eqref{eq:wstep_pochodna_granica} i przedstawia pochodną jako \emph{infinitezymalny} iloraz różnicowy, czyli stosunek \emph{infinitezymalnego} przyrostu funkcji,~$\dd{f(x)}$, do \emph{infinitezymalnego} przyrostu argumentu,~$\dd{x}$. Te nieskończenie małe przyrosty nazywamy~\emph{różniczkami}. Zapis~\eqref{eq:wstep_pochodna_rozniczki} pozwala też obliczyć infinitezymalny przyrost funkcji w danym punkcie,
\begin{equation}
	\dd{f(x_0)} = f'(x_0) \dd{x}.
\end{equation}

Wyznaczmy pochodne kilku prostych funkcji korzystając z~\eqref{eq:wstep_pochodna_granica}. Dla wyznaczenia pochodnych funkcji~$\sin{x}$ i~$\cos{x}$ potrzebne nam będą podstawowe zależności trygonometryczne,
\begin{align}
	\sin{(\alpha + \beta)} & = \sin{\alpha} \cos{\beta} + \cos{\alpha} \sin{\beta}, \\
	\cos{(\alpha + \beta)} & = \cos{\alpha} \cos{\beta} - \sin{\alpha} \sin{\beta},
\end{align}
granice, które możemy odczytać z wykresu~\ref{fig:wstep_sin_cos}:
\begin{align}
	\lim_{x \to 0} \sin{x} & = 0, \\
	\lim_{x \to 0} \cos{x} & = 1,
\end{align}
i wreszcie wartości mniej trywialnych granic,
\begin{align}
	& \lim_{x \to 0} \frac{\sin{x}}{x} = 1, \\
	& \lim_{x \to 0} \frac{1 - \cos{x}}{x} = 1.
\end{align}
Wyprowadzenie kilku pochodnych znajduje się w tab.~\ref{tab:wstep_podstawowe_pochodne}.
\begingroup
	\addtolength{\tabcolsep}{6pt}
	\renewcommand{\arraystretch}{1.5}
	\begin{table}[H]
		\centering
		\caption{Wyprowadzenie pochodnych podstawowych funkcji}
			\begin{tabular}{l*{2}{>{$}l<{$}}>{$}r<{$}}
				\toprule
				funkcja & f(x) & \bar{v}_f(x, \Delta x) & f'(x) \\
				\midrule
				stała & a & \frac{a - a}{\Delta x} = 0 & 0 \\
				liniowa & a x + b & \frac{a (x + \Delta x) + b - (a x + b)}{\Delta x} = a & a \\
				kwadratowa & x^2 & \frac{(x + \Delta x)^2 - x^2}{\Delta x} = 2 x + \Delta x & 2 x \\
				homograficzna & \frac{1}{x} & \frac{1}{\Delta x} \left( \frac{1}{x + \Delta x} - \frac{1}{x} \right) = -\frac{1}{x (x + \Delta x)} & -\frac{1}{x^2} \\
				pierwiastek & \sqrt{x} & \frac{\sqrt{x + \Delta x} - \sqrt{x}}{\Delta x} = \frac{1}{\sqrt{x + \Delta x} + \sqrt{x}} & \frac{1}{2 \sqrt{x}} \\
				sinus & \sin{x} & \frac{\sin{(x + \Delta x)} - \sin{x}}{\Delta x} = \frac{\sin{x} (\cos{\Delta x} - 1) + \cos{x} \sin{\Delta x}}{\Delta x} & \cos{x} \\
				cosinus & \cos{x} & \frac{\cos{(x + \Delta x)} - \cos{x}}{\Delta x} = \frac{\cos{x} (\cos{\Delta x} - 1) - \sin{x} \sin{\Delta x}}{\Delta x} & -\sin{x} \\
				\bottomrule
			\end{tabular}
			\label{tab:wstep_podstawowe_pochodne}
	\end{table}
\endgroup
Zwróćmy jeszcze uwagę, że o ile w matematyce zwykle różniczkujemy po zmiennej~$x$, o tyle w fizyce czynimy to zwykle po czasie,~$t$, np. szybkość to pochodna drogi po czasie:
\begin{align}
	v(t) & = \dv{s(t)}{t} \\
	 & = \dot{s}(t) \label{eq:wstep_pochodna_po_czasie_kropka},
\end{align}
przy czym pochodne po czasie często oznacza się kropką, jak w~\eqref{eq:wstep_pochodna_po_czasie_kropka}.

\subsection{Całki}

Całkowanie jest procesem odwrotnym do różniczkowania. \emph{Całka nieoznaczona} jest \emph{funkcją pierwotną} danej funkcji:
\begin{equation}
	\int f(x) = F(x) \iff \dv{F(x)}{x} = f(x).
\end{equation}

\subsubsection{Długość krzywej}

Rozpatrzmy krzywą~$C$ w przestrzeni, o końcach w punktach~$\vb{r}_a$ i~$\vb{r}_b$. Chcemy znaleźć długość~$s$ tej krzywej. W tym celu przybliżamy najpierw krzywą przez~$n$ odcinków tak, że~$i$-ty odcinek o długości~$\Delta s_i$ zawarty jest między punktami~$\vb{r}_i$ i~$\vb{r}_{i + 1}$, przy czym~$\vb{r}_a = \vb{r}_1$ i~$\vb{r}_b = \vb{r}_{n + 1}$ (by podzielić krzywą na~$n$ odcinków, musimy na niej zaznaczyć~$n + 1$ punktów). Zachodzi wówczas
\begin{align}
	\Delta s_i & = \norm{\Delta \vb{r}_i} \nonumber \\
	& = \norm{\vb{r}_{i + 1} - \vb{r}_i} \nonumber \\
	& = \sqrt{(x_{i + 1} - x_i)^2 + (y_{i + 1} - y_i)^2 + (z_{i + 1} - z_i)^2} \nonumber \\
	& = \sqrt{(\Delta x_i)^2 + (\Delta y_i)^2 + (\Delta z_i)^2} \label{eq:hmty}
\end{align}
oraz
\begin{equation}
	s \approx \sum_{i = 1}^n {\Delta s_i}.
	\label{eq:pdud}
\end{equation}
Przechodząc do granicy~$\Delta s_i \to 0$ dla największego~$\Delta s_i$ uzyskujemy ścisłą długość krzywej, która jest całką
\begin{align}
	s & = \int_C \, \dd s \nonumber \\
	& = \int_C \, \sqrt{(\dd x)^2 + (\dd y)^2 + (\dd z)^2}. \label{eq:cbss}
\end{align}

Całkę~\eqref{eq:cbss} można w ogólności wyznaczyć parametryzując krzywą~$C$, tj. wprowadzając zmienną, od której zależy wektor położenia:
\begin{equation}
	C: \, \vb{r}(t), \, t \in \interval{t_a}{t_b}. \label{quhz}
\end{equation}
Wówczas np. dla składowej~$x$-owej zachodzi
\begin{align}
	x'(t) & \equiv \dot{x}(t) = \dv{x}{t},
	\label{eq:flwk}
\end{align}
gdzie wprowadziliśmy oznaczenie pochodnej po czasie,~$\dot{x}(t)$, bowiem w fizyce oczywistym wyborem parametru~$t$ jest czas. Zakładając, że~$t_b > t_a$, z~\eqref{eq:cbss} i~\eqref{eq:flwk} uzyskujemy
\begin{equation}
	s = \int_{t_a}^{t_b} \sqrt{\Big( \dot{x}(t) \Big)^2 + \Big( \dot{y}(t) \Big)^2 + \Big( \dot{z}(t) \Big)^2} \, \dd t.
	\label{eq:btmk}
\end{equation}
Gdy~$t$ jest czasem, pochodne w całce~\eqref{eq:btmk} są z definicji składowymi wektora prędkości,
\begin{equation}
	\vb{v}(t) = \dot{\vb{r}}(t).
	\label{eq:cmzl}
\end{equation}
Łącząc~\eqref{eq:wstep_norma_wekt},~\eqref{eq:btmk} i~\eqref{eq:cmzl} uzyskujemy
\begin{equation}
	\tcboxmath{s = \int_{t_a}^{t_b} v(t) \, \dd t}.
	\label{eq:tihe}
\end{equation}

Rozpatrzmy jeszcze przypadek dwuwymiarowy, gdy interesuje nas długość wykresu funkcji~$y = f(x)$ dla~$x \in \interval{x_a}{x_b}$. Korzystając z zależności~\eqref{eq:btmk} (w której oczywiście pomijamy składową~$z$-ową), stosując oczywistą parametryzację
\begin{align}
	x(t) & = t, \\
	y(t) & = f(t),
\end{align}
dla której
\begin{align}
	\dot{x}(t) & = 1, \\
	\dot{y}(t) & = f'(t),
\end{align}
po zmianie zmiennej całkowania z~$t$ na~$x$ (nazwa zmiennej, po której całkujemy, nie ma bowiem znaczenia), uzyskujemy ostatecznie
\begin{equation}
	\tcboxmath{s = \int_{x_a}^{x_b} \sqrt{1 + \Big( f'(x) \Big)^2} \, \dd x}.
	\label{eq:tdhp}
\end{equation}

\section{Podstawy rachunku błędów}

Jeśli przy $i$-tym~pomiarze wielkości~$x$ uzyskaliśmy wartość~$x_i$, wówczas \emph{błąd pomiaru} wynosi
\begin{equation}
	\Delta x_i = x_i - x_0,
\end{equation}
gdzie~$x_0$ jest rzeczywistą wartością wielkości. Ponieważ zwykle nie znamy rzeczywistej wartości, nie znamy też błędu. Możemy jednak na podstawie zbioru wartości~$\{x_i\}_{i = 1}^n$ $n$~pomiarów określić jakościowo rodzaj błędu:
\begin{itemize}
	\item błąd przypadkowy, gdy wartości rozrzucone są losowo wokół wartości rzeczywistej.
	\item błąd systematyczny, gdy wartości skupiają się wokół pewnej wartości, zwykle różnej od wartości rzeczywistej.
	\item błąd gruby, gdy jedna lub więcej wartości drastycznie odbiega od pozostałych wartości.
\end{itemize}
Celem rachunku błędów jest wyznaczenie \emph{niepewności} pomiaru mierzonej wielkości.

Zakładamy, że błąd przypadkowy ma \emph{normalny} rozrzut wartości. \emph{Rozkład normalny} pojawia się bardzo często w życiu codziennym: jeśli wybierzemy losowo~$1000$ osób i zmierzymy ich wzrost, otrzymamy rozkład mniej więcej taki, jak na rys.~\ref{fig:rozklad_wzrostu}
\begin{figure}[h!]
	\centering
	\includegraphics{Wstep/height_distribution}
	\caption{Rozkład wzrostu}
	\label{fig:rozklad_wzrostu}
\end{figure}

W przypadku występowania błędu przypadkowego stosujemy statystyczną ocenę wyników pomiarów: szukaną wartość utożsamiamy ze średnią:
\begin{equation}
	\bar{x} = \frac{1}{n} \sum_{i = 1}^n x_i,
\end{equation}
a niepewność pomiaru --- z estymatorem odchylenia standardowego średniej:
\begin{equation}
	s_{\bar{x}} = \sqrt{\frac{1}{n (n - 1)} \sum_{i = 1}^n {\left( x_i - \bar{x} \right)^2}}.
\end{equation}

\nocite{isbn:8301123583}
\printbibliography[heading = subbibliography]
